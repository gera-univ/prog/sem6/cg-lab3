CC=cc
GIMPTOOL=gimptool-2.0

utils: histogram linear-contrast

histogram: histogram.c farbfeld.c
	$(CC) $^ -o $@

linear-contrast: linear-contrast.c farbfeld.c
	$(CC) $^ -o $@

install-plugins: gimp-equalizer gimp-linear-contrast

gimp-equalizer: gimp-equalizer.c
	$(GIMPTOOL) --install $^

gimp-linear-contrast: gimp-linear-contrast.c
	$(GIMPTOOL) --install $^

example/blood.ff: example/blood.jpg
	jpg2ff <$< >$@
	
example/blood_equalized.ff: example/blood.ff histogram
	./histogram $< $@

example/blood_contrasted.ff: example/blood.ff linear-contrast
	./linear-contrast $< $@

example: example/blood.ff example/blood_equalized.ff example/blood_contrasted.ff

.PHONY: clean example install-plugins utils

clean:
	-rm example/blood_equalized.ff
	-rm example/blood_contrasted.ff
	-rm example/blood.ff
	-rm histogram
	-rm linear-contrast

uninstall:
	$(GIMPTOOL) --uninstall-bin gimp-equalizer
	$(GIMPTOOL) --uninstall-bin gimp-linear-contrast
