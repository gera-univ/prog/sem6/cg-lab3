# Utils

* `histogram` - displays image histogram and outputs equalized image
* `linear-contrast` - applies linear contrasting to image, displays max/min color for each channel

# [GIMP](https://www.gimp.org/) Plugins

* gera's Equalizer - equalizes selected region
* gera's Linear Contrast - applies linear contrasting to selected region

![Plugin demonstration](demo.gif)

# Installation

```
$ make utils
$ make install-plugins
$ make example # requires farbfeld [1] encoder
```
[[1]](https://tools.suckless.org/farbfeld/)
