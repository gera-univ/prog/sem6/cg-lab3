#include "farbfeld.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <arpa/inet.h>

struct FarbfeldHeader
{
	char sig[8];
	uint32_t width;
	uint32_t height;
};

bool
farbfeld_read(const char *filename, Farbfeld *f)
{
	bool result = false;
	FILE *img = fopen(filename, "rb");
	if (img == NULL)
		return false;

	struct FarbfeldHeader hdr;
	if (fread(&hdr.sig, sizeof (hdr), 1, img) != 1)
		goto cleanup;
	if (memcmp(hdr.sig, "farbfeld", sizeof ("farbfeld")) != 0)
		goto cleanup;

	f->width = ntohl(hdr.width);
	f->height = ntohl(hdr.height);
	uint64_t n_pixels = f->width * f->height;

	f->data = calloc(n_pixels, sizeof (FarbfeldPixel));
	if (f->data == NULL)
		goto cleanup;

	if (fread(f->data, sizeof (FarbfeldPixel), n_pixels, img) != n_pixels)
	{
		free(f->data);
		goto cleanup;
	}

	for (uint64_t i = 0; i < n_pixels; ++i)
	{
		f->data[i].r = ntohs(f->data[i].r);
		f->data[i].g = ntohs(f->data[i].g);
		f->data[i].b = ntohs(f->data[i].b);
		f->data[i].a = ntohs(f->data[i].a);
	}

	result = true;

cleanup:
	fclose(img);
	return result;
}

bool
farbfeld_write(const char *filename, Farbfeld *f)
{
	FarbfeldPixel *data = NULL;
	bool result = false;
	FILE *img = fopen(filename, "wb");
	if (img == NULL)
		return false;

	struct FarbfeldHeader hdr = {
		.sig="farbfeld",
		.width = htonl(f->width),
		.height = htonl(f->height),
	};
	if (fwrite(&hdr.sig, sizeof (hdr), 1, img) != 1)
		goto cleanup;

	uint64_t n_pixels = f->width * f->height;
	data = calloc(n_pixels, sizeof (FarbfeldPixel));

	for (uint64_t i = 0; i < n_pixels; ++i)
	{
		data[i].r = htons(f->data[i].r);
		data[i].g = htons(f->data[i].g);
		data[i].b = htons(f->data[i].b);
		data[i].a = htons(f->data[i].a);
	}

	if (fwrite(data, sizeof (FarbfeldPixel), n_pixels, img) != n_pixels)
	{
		free(data);
		goto cleanup;
	}

	result = true;

cleanup:
	fclose(img);
	free(data);
	return result;
}

void farbfeld_setpixel(Farbfeld *f, uint32_t x, uint32_t y, FarbfeldPixel p)
{
	f->data[y * f->width + x] = p;
}

FarbfeldPixel farbfeld_getpixel(const Farbfeld *f, uint32_t x, uint32_t y)
{
	return f->data[y * f->width + x];
}

void farbfeld_delete(Farbfeld *f)
{
	free(f->data);
}
