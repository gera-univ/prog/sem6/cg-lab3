#pragma once

#include <stdbool.h>
#include <stdint.h>

typedef struct
{
	uint16_t r;
	uint16_t g;
	uint16_t b;
	uint16_t a;
} FarbfeldPixel;

typedef struct
{
	uint32_t width;
	uint32_t height;
	FarbfeldPixel *data;
} Farbfeld;

bool farbfeld_read(const char *filename, Farbfeld *f_img);
bool farbfeld_write(const char *filename, Farbfeld *f_img);
void farbfeld_setpixel(Farbfeld *f, uint32_t x, uint32_t y, FarbfeldPixel p);
FarbfeldPixel farbfeld_getpixel(const Farbfeld *f, uint32_t x, uint32_t y);
void farbfeld_delete(Farbfeld *f);
