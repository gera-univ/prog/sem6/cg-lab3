#include <libgimp/gimp.h>

void run (const gchar      *name,
          gint              nparams,
          const GimpParam  *param,
          gint             *nreturn_vals,
          GimpParam       **return_vals);

static void query (void);
static void process (GimpDrawable *drawable);

GimpPlugInInfo PLUG_IN_INFO = {
	NULL,
	NULL,
	query,
	run
};

MAIN();

static void query (void)
{
	static GimpParamDef args[] = {
		{
			GIMP_PDB_INT32,
			"run-mode",
			"Run mode"
		},
		{
			GIMP_PDB_IMAGE,
			"image",
			"Input image"
		},
		{
			GIMP_PDB_DRAWABLE,
			"drawable",
			"Input drawable"
		}
	};

	gimp_install_procedure(
		"gera-equalizer",
		"Histogram Equalizer",
		"Equalizes the image histogram",
		"gera",
		"Public Domain",
		"2021",
		"gera's Equalizer",
		"RGB*, GRAY*",
		GIMP_PLUGIN,
		G_N_ELEMENTS (args), 0,
		args, NULL
	);

	gimp_plugin_menu_register("gera-equalizer", "<Image>/Filters/Enhance");
}

void run (const gchar      *name,
          gint              nparams,
          const GimpParam  *param,
          gint             *nreturn_vals,
          GimpParam       **return_vals)
{
	static GimpParam  values[1];
	GimpPDBStatusType status = GIMP_PDB_SUCCESS;
	GimpRunMode       run_mode;

	*nreturn_vals = 1;
	*return_vals  = values;

	values[0].type = GIMP_PDB_STATUS;
	values[0].data.d_status = status;

	run_mode = param[0].data.d_int32;

	GimpDrawable *drawable = gimp_drawable_get(param[2].data.d_drawable);

	process(drawable);

	gimp_displays_flush();
	gimp_drawable_detach(drawable);

	return;
}

static void process (GimpDrawable *drawable)
{
	gint x1, y1, x2, y2;
	gimp_drawable_mask_bounds(drawable->drawable_id, &x1, &y1, &x2, &y2);
	gint channels = gimp_drawable_bpp(drawable->drawable_id);

	GimpPixelRgn rgn_in, rgn_out;
	gimp_pixel_rgn_init(&rgn_in, drawable, x1, y1, x2 - x1, y2 - y1, FALSE, FALSE);
	gimp_pixel_rgn_init(&rgn_out, drawable, x1, y1, x2 - x1, y2 - y1, TRUE, TRUE);

	guchar *row = g_new(guchar, channels * (x2 - x1));
	guchar *outrow = g_new(guchar, channels * (x2 - x1));

	gint n_pixels = 0;
	float *histogram = g_new0(float, 255);
	for (gint i = y1; i < y2; i++)
	{
		gimp_pixel_rgn_get_row(&rgn_in, row, x1, i, x2 - x1);
		for (gint j = x1; j < x2; j++)
		{
			++n_pixels;
			for (gint k = 0; k < channels; k++)
			{
				gint index = channels * (j - x1) + k;
				histogram[row[channels * (j - x1) + k]] += 1.0f/channels;
			}
		}
	}

	float *cdf = g_new(float, 255);
	cdf[0] = histogram[0] / (float)n_pixels; 
	for (gint i = 1; i < 255; ++i) {
		cdf[i] = cdf[i - 1] + histogram[i] / (float)n_pixels;
	}

	for (gint i = y1; i < y2; i++)
	{
		gimp_pixel_rgn_get_row(&rgn_in, row, x1, i, x2 - x1);
		for (gint j = x1; j < x2; j++)
		{
			for (gint k = 0; k < channels; k++)
			{
				gint index = channels * (j - x1) + k;
				outrow[index] = (guchar)(255 * cdf[row[index]]);
			}
		}
		gimp_pixel_rgn_set_row(&rgn_out, outrow, x1, i, x2 - x1);
		if (i % 10 == 0)
			gimp_progress_update ((gdouble) (i - y1) / (gdouble) (y2 - y1));
	}

	g_free(row);
	g_free(outrow);
	g_free(histogram);
	g_free(cdf);

	gimp_drawable_flush(drawable);
	gimp_drawable_merge_shadow(drawable->drawable_id, TRUE);
	gimp_drawable_update(drawable->drawable_id, x1, y1, x2 - x1, y2 - y1);
}
