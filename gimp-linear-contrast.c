#include <libgimp/gimp.h>

void run (const gchar      *name,
          gint              nparams,
          const GimpParam  *param,
          gint             *nreturn_vals,
          GimpParam       **return_vals);

static void query (void);
static void process (GimpDrawable *drawable);

GimpPlugInInfo PLUG_IN_INFO = {
	NULL,
	NULL,
	query,
	run
};

MAIN();

static void query (void)
{
	static GimpParamDef args[] = {
		{
			GIMP_PDB_INT32,
			"run-mode",
			"Run mode"
		},
		{
			GIMP_PDB_IMAGE,
			"image",
			"Input image"
		},
		{
			GIMP_PDB_DRAWABLE,
			"drawable",
			"Input drawable"
		}
	};

	gimp_install_procedure(
		"gera-linear-contrast",
		"Linear Contrast",
		"Applies linear contrasting",
		"gera",
		"Public Domain",
		"2021",
		"gera's Linear Contrast",
		"RGB*, GRAY*",
		GIMP_PLUGIN,
		G_N_ELEMENTS (args), 0,
		args, NULL
	);

	gimp_plugin_menu_register("gera-linear-contrast", "<Image>/Filters/Enhance");
}

void run (const gchar      *name,
          gint              nparams,
          const GimpParam  *param,
          gint             *nreturn_vals,
          GimpParam       **return_vals)
{
	static GimpParam  values[1];
	GimpPDBStatusType status = GIMP_PDB_SUCCESS;
	GimpRunMode       run_mode;

	*nreturn_vals = 1;
	*return_vals  = values;

	values[0].type = GIMP_PDB_STATUS;
	values[0].data.d_status = status;

	run_mode = param[0].data.d_int32;

	GimpDrawable *drawable = gimp_drawable_get(param[2].data.d_drawable);

	process(drawable);

	gimp_displays_flush();
	gimp_drawable_detach(drawable);

	return;
}

static void process (GimpDrawable *drawable)
{
	gint x1, y1, x2, y2;
	gimp_drawable_mask_bounds(drawable->drawable_id, &x1, &y1, &x2, &y2);
	gint channels = gimp_drawable_bpp(drawable->drawable_id);

	GimpPixelRgn rgn_in, rgn_out;
	gimp_pixel_rgn_init(&rgn_in, drawable, x1, y1, x2 - x1, y2 - y1, FALSE, FALSE);
	gimp_pixel_rgn_init(&rgn_out, drawable, x1, y1, x2 - x1, y2 - y1, TRUE, TRUE);

	guchar *row = g_new(guchar, channels * (x2 - x1));
	guchar *outrow = g_new(guchar, channels * (x2 - x1));

	guchar min_channel[channels], max_channel[channels];

	gint n_pixels = 0;
	for (gint i = y1; i < y2; i++)
	{
		gimp_pixel_rgn_get_row(&rgn_in, row, x1, i, x2 - x1);
		if (i == 0)
		{
			for (gint k = 0; k < channels; k++)
			{
				min_channel[k] = row[k];
				max_channel[k] = row[k];
			}
		}
		for (gint j = x1; j < x2; j++)
		{
			++n_pixels;
			for (gint k = 0; k < channels; k++)
			{
				gint index = channels * (j - x1) + k;
				if (row[index] < min_channel[k]) min_channel[k] = row[index];
				if (row[index] > max_channel[k]) max_channel[k] = row[index];
			}
		}
	}

	//for (gint k = 0; k < channels; k++)
	//	g_print("min_channel[%d]  %d\tmax_channel[%d]  %d\n",
	//			k, min_channel[k], k, max_channel[k]);

	for (gint i = y1; i < y2; i++)
	{
		gimp_pixel_rgn_get_row(&rgn_in, row, x1, i, x2 - x1);
		for (gint j = x1; j < x2; j++)
		{
			for (gint k = 0; k < channels; k++)
			{
				gint index = channels * (j - x1) + k;
				outrow[index] = 255 / (max_channel[k] - min_channel[k]) * (row[index] - min_channel[k]);
			}
		}
		gimp_pixel_rgn_set_row(&rgn_out, outrow, x1, i, x2 - x1);
		if (i % 10 == 0)
			gimp_progress_update ((gdouble) (i - y1) / (gdouble) (y2 - y1));
	}

	g_free(row);
	g_free(outrow);

	gimp_drawable_flush(drawable);
	gimp_drawable_merge_shadow(drawable->drawable_id, TRUE);
	gimp_drawable_update(drawable->drawable_id, x1, y1, x2 - x1, y2 - y1);
}
