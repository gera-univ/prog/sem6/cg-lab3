#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "farbfeld.h"

#define SAMPLES 128
#define HEIGHT 16
#define SYMB '#'

void usage(char *argv0)
{
	fprintf(stderr, "usage: %s image.ff [equalized.ff]\n", argv0);
	exit(1);
}

void print_histogram(size_t histogram[], uint64_t n_colors, size_t sample_count,
                     size_t height, char symb)
{
	size_t sample_array[sample_count];
	memset(sample_array, 0, sizeof (sample_array));
	for (uint64_t i = 0; i < n_colors; ++i) {
		sample_array[(sample_count * i) / n_colors] += histogram[i];
	}

	size_t maximum = 0;
	for (int i = 0; i < sample_count; ++i) {
		if (sample_array[i] > maximum)
			maximum = sample_array[i];
	}
	
	for (int i = 0; i < sample_count; ++i) {
		sample_array[i] = sample_array[i] / (float)(maximum) * height;
	}

	for (int i = 0; i < height; ++i) {
		for (int j = 0; j < sample_count; ++j) {
			if (sample_array[j] >= (height - i))
				printf("%c", symb);
			else
				printf(" ");
		}
		printf("|\n");
	}
}

void compute_histogram(size_t histogram[], uint64_t n_pixels,
                       uint16_t channel[])
{
	for (uint64_t j = 0; j < n_pixels; j++)
		++histogram[channel[j]];
}

void compute_cdf(double cdf[], uint64_t n_pixels,
                 size_t hr[], size_t hg[], size_t hb[], size_t n_colors)
{
	cdf[0] = (hr[0] + hg[0] + hb[0]) / (3.0*n_pixels);
	for (uint64_t i = 1; i < n_colors; ++i)
		cdf[i] = cdf[i - 1] + (hr[i] + hg[i] + hb[i]) / (3.0*n_pixels);
}

void equalize_channel(uint16_t channel[], uint64_t n_pixels,
                      double cdf[], size_t n_colors)
{
	for (uint64_t i = 0; i < n_pixels; ++i)
		channel[i] = n_colors * cdf[channel[i]];
}

int main(int argc, char *argv[])
{
	char *argv0 = argv[0]; --argc; ++argv;
	char *filename = argv[0]; --argc; ++argv;
	char *out_filename = NULL;
	if (argc != 0)
	{
		out_filename = argv[0]; --argc; ++argv;

		if (argc != 0)
		{
			usage(argv0);
		}
	}

	Farbfeld img;
	if (farbfeld_read(filename, &img) != true)
	{
		perror("farbfeld_read");
		exit(2);
	}

	uint64_t n_pixels = img.width * img.height;
	size_t n_colors = 1 << 16;

	uint16_t *r = calloc(n_pixels, sizeof (uint16_t));
	uint16_t *g = calloc(n_pixels, sizeof (uint16_t));
	uint16_t *b = calloc(n_pixels, sizeof (uint16_t));
	uint16_t *a = calloc(n_pixels, sizeof (uint16_t));
	for (uint32_t i = 0; i < img.height; i++) {
		for (uint32_t j = 0; j < img.width; j++) {
			FarbfeldPixel p = farbfeld_getpixel(&img, j, i);
			r[i*img.width + j] = p.r;
			g[i*img.width + j] = p.g;
			b[i*img.width + j] = p.b;
			a[i*img.width + j] = p.a;
		}
	}

	size_t *rcounts = calloc(n_colors, sizeof (size_t));
	size_t *gcounts = calloc(n_colors, sizeof (size_t));
	size_t *bcounts = calloc(n_colors, sizeof (size_t));
	size_t *acounts = calloc(n_colors, sizeof (size_t));
	compute_histogram(rcounts, n_pixels, r);
	compute_histogram(gcounts, n_pixels, g);
	compute_histogram(bcounts, n_pixels, b);
	compute_histogram(acounts, n_pixels, a);

	// equalize image
	if (out_filename != NULL) {
		double *cdf = calloc(n_colors, sizeof (double));
		compute_cdf(cdf, n_pixels, rcounts, gcounts, bcounts, n_colors);
		equalize_channel(r, n_pixels, cdf, n_colors);
		equalize_channel(g, n_pixels, cdf, n_colors);
		equalize_channel(b, n_pixels, cdf, n_colors);

		for (uint32_t i = 0; i < img.height; i++) {
			for (uint32_t j = 0; j < img.width; j++) {
				FarbfeldPixel p = {
					r[i*img.width + j],
					g[i*img.width + j],
					b[i*img.width + j],
					a[i*img.width + j]
				};
				farbfeld_setpixel(&img, j, i, p);
			}
		}

		farbfeld_write(out_filename, &img);
		free(cdf);

		compute_histogram(rcounts, n_pixels, r);
		compute_histogram(gcounts, n_pixels, g);
		compute_histogram(bcounts, n_pixels, b);
		compute_histogram(acounts, n_pixels, a);
	}

	printf("ALPHA\n");
	print_histogram(acounts, n_colors, SAMPLES, 16, '#');
	printf("RED\n");
	print_histogram(rcounts, n_colors, SAMPLES, 16, '#');
	printf("GREEN\n");
	print_histogram(gcounts, n_colors, SAMPLES, 16, '#');
	printf("BLUE\n");
	print_histogram(bcounts, n_colors, SAMPLES, 16, '#');
	free(r);
	free(g);
	free(b);
	free(a);
	free(rcounts);
	free(gcounts);
	free(bcounts);
	free(acounts);
	farbfeld_delete(&img);
	return 0;
}
