#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "farbfeld.h"

void usage(char *argv0)
{
	fprintf(stderr, "usage: %s image.ff [contrasted.ff]\n", argv0);
	exit(1);
}

int main(int argc, char *argv[])
{
	char *argv0 = argv[0]; --argc; ++argv;
	char *filename = argv[0]; --argc; ++argv;
	char *out_filename = NULL;
	if (argc != 0)
	{
		out_filename = argv[0]; --argc; ++argv;

		if (argc != 0)
		{
			usage(argv0);
		}
	}

	Farbfeld img;
	if (farbfeld_read(filename, &img) != true)
	{
		perror("farbfeld_read");
		exit(2);
	}

	uint64_t n_pixels = img.width * img.height;
	size_t n_colors = 1 << 16;

	uint16_t *r = calloc(n_pixels, sizeof (uint16_t));
	uint16_t *g = calloc(n_pixels, sizeof (uint16_t));
	uint16_t *b = calloc(n_pixels, sizeof (uint16_t));
	uint16_t *a = calloc(n_pixels, sizeof (uint16_t));
	for (uint32_t i = 0; i < img.height; i++) {
		for (uint32_t j = 0; j < img.width; j++) {
			FarbfeldPixel p = farbfeld_getpixel(&img, j, i);
			r[i*img.width + j] = p.r;
			g[i*img.width + j] = p.g;
			b[i*img.width + j] = p.b;
			a[i*img.width + j] = p.a;
		}
	}
	
	uint16_t fmin_r = r[0], fmax_r = r[0];
	uint16_t fmin_g = g[0], fmax_g = g[0];
	uint16_t fmin_b = b[0], fmax_b = b[0];
	for (uint32_t i = 0; i < img.height; i++) {
		for (uint32_t j = 0; j < img.width; j++) {
			uint64_t index = i*img.width + j;
			if (r[index] < fmin_r) fmin_r = r[index];
			if (r[index] > fmax_r) fmax_r = r[index];
			if (g[index] < fmin_g) fmin_g = g[index];
			if (g[index] > fmax_g) fmax_g = g[index];
			if (b[index] < fmin_b) fmin_b = b[index];
			if (b[index] > fmax_b) fmax_b = b[index];
		}
	}

	printf("fmin_r  %d\tfmax_r  %d\n", fmin_r, fmax_r);
	printf("fmin_g  %d\tfmax_g  %d\n", fmin_g, fmax_g);
	printf("fmin_b  %d\tfmax_b  %d\n", fmin_b, fmax_b);
	printf("color_count\t\t%lu\n", n_colors);

	if (out_filename != NULL) {
		for (uint32_t i = 0; i < img.height; i++) {
			for (uint32_t j = 0; j < img.width; j++) {
				uint64_t index = i*img.width + j;
				uint16_t red   = n_colors / (fmax_r - fmin_r) * (r[index] - fmin_r);
				uint16_t green = n_colors / (fmax_g - fmin_g) * (g[index] - fmin_g);
				uint16_t blue  = n_colors / (fmax_b - fmin_b) * (b[index] - fmin_b);
				FarbfeldPixel p = {
					red,
					green,
					blue,
					a[index]
				};
				farbfeld_setpixel(&img, j, i, p);
			}
		}

		farbfeld_write(out_filename, &img);
	}

	free(r);
	free(g);
	free(b);
	free(a);
	farbfeld_delete(&img);
	return 0;
}
